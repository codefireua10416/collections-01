/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        PhoneFactory factory = new NokiaFactory();
        Phone makeMobilePhone = factory.makeMobilePhone("+389773289472");
        System.out.println(makeMobilePhone);
        
        factory = new SamsungFactory();
        makeMobilePhone = factory.makeMobilePhone("+389778759472");
        System.out.println(makeMobilePhone);
    }
    
}
