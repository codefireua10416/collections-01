/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author human
 */
public class MobilePhone extends Phone {
    
    private int simCards;

    public MobilePhone(int simCards, String number, double displaySize, int keypad) {
        super(number, displaySize, keypad);
        this.simCards = simCards;
    }

    @Override
    public String toString() {
        return "MobilePhone{" + "simCards=" + simCards + '}' + super.toString();
    }
   
}
