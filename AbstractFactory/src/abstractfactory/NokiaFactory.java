/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author human
 */
public class NokiaFactory extends PhoneFactory {

    private static final String BRAND_NAME = "Nokia";

    @Override
    public Phone makeMobilePhone(String number) {
        MobilePhone mobilePhone = new MobilePhone(2, number, 2.5, 12);
        mobilePhone.setBrand(BRAND_NAME);

        return mobilePhone;
    }

    @Override
    public Phone makeSmartPhone(String number) {
        SmartPhone smartPhone = new SmartPhone(5.0, "Android", number, 4.5, 4);
        smartPhone.setBrand(BRAND_NAME);

        return smartPhone;
    }

}
