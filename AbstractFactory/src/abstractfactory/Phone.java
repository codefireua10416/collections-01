/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author human
 */
public abstract class Phone {

    private String brand;
    private String number;
    private double displaySize;
    private int keypad;

    public Phone(String number, double displaySize, int keypad) {
        this.number = number;
        this.displaySize = displaySize;
        this.keypad = keypad;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getNumber() {
        return number;
    }

    public double getDisplaySize() {
        return displaySize;
    }

    public int getKeypad() {
        return keypad;
    }

    @Override
    public String toString() {
        return "Phone{" + "brand=" + brand + ", number=" + number + ", displaySize=" + displaySize + ", keypad=" + keypad + '}';
    }

}
