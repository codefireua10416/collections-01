/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author human
 */
public abstract class PhoneFactory {

    public abstract Phone makeMobilePhone(String number);

    public abstract Phone makeSmartPhone(String number);

}
