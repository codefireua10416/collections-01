/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author human
 */
public class SamsungFactory extends PhoneFactory {

    @Override
    public Phone makeMobilePhone(String number) {
        return new MobilePhone(1, number, 2.2, 15);
    }

    @Override
    public Phone makeSmartPhone(String number) {
        return new SmartPhone(3.2, "Android", number, 5.5, 5);
    }
    
}
