/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author human
 */
public class SmartPhone extends Phone {
    
    private double camera;
    private String os;

    public SmartPhone(double camera, String os, String number, double displaySize, int keypad) {
        super(number, displaySize, keypad);
        this.camera = camera;
        this.os = os;
    }

    public double getCamera() {
        return camera;
    }

    public String getOs() {
        return os;
    }

    @Override
    public String toString() {
        return "SmartPhone{" + "camera=" + camera + ", os=" + os + '}';
    }
    
}
