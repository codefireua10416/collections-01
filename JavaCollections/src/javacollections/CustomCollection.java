/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javacollections;

/**
 *
 * @author human
 */
public class CustomCollection<T> {

    private Object[] data;
    private int position = 0;

    public CustomCollection(int size) {
        this.data = new Object[size];
    }

    public Object[] getData() {
        return data;
    }

    public boolean add(T obj) {
//        if (position < capacity)
        if (position < data.length) {
            data[position++] = obj;
            return true;
        }

        return false;
    }
}
