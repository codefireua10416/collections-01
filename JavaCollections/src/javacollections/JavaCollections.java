/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javacollections;

import java.util.Arrays;

/**
 *
 * @author human
 */
public class JavaCollections {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CustomCollection<String> collection1 = new CustomCollection<>(5);
        collection1.add("One");
        collection1.add("Two");
        collection1.add("Three");
        collection1.add("Four");
        System.out.println(collection1.add("Five"));
        System.out.println(collection1.add("Six"));
        System.out.println(collection1.add("Seven"));
        System.out.println(collection1.add("Eight"));
        
        Object[] data = collection1.getData();
        System.out.println(Arrays.toString(data));
    }
    
}
